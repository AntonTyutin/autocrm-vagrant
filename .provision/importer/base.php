<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',10);

return [
    'runtimePath' => '/tmp',
    'params' => [
        'reference_database_name' => 'autocrm',
//        'logo_file' => 'logo-peugeot.png',
        'stock_load' => [
            'loaders' => ['Ford'],
            'archive_dir' => '/tmp/tyutin/downloads',
        ]
    ],
    'components' => [
        'db' => [
            'connectionString'  => 'mysql:host=autocrm.dev;dbname=autocrm_gm',
            'username'          => 'root',
            'password'          => '',

            'emulatePrepare'    => true,
            'charset'           => 'utf8',
//            'enableProfiling'   => true, // включаем профайлер
            'enableParamLogging' => true, // показываем значения параметров
        ],
    ]
];
