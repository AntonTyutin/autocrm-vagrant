<?php

return [
    'commandMap' => [
        'migrate' => [
            'interactive' => false
        ]
    ],
    'components' => [
        'mail' => [ // ext.yii-mail.YiiMail
            'dryRun' => true
        ],
    ]
];
