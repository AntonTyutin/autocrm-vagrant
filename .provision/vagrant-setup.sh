#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
PROVISION_DIR=/vagrant/.provision
LOGDIR=/var/log/www
DOMAIN=autocrm.dev

echo 'Установка основных компонентов'
apt-get -yq update \
    && apt-get install -yq \
        apache2 libapache2-mod-php5 \
        php5-mysql php5-sqlite php5-json php5-gd php5-xcache php5-xdebug php5-intl \
        mysql-server phpmyadmin libmysqlclient-dev \
        mc git-core

echo 'Создание директории для логов'
mkdir -p $LOGDIR && chown www-data:www-data $LOGDIR
grep "$LOGDIR" /etc/fstab >/dev/null || ( echo "none $LOGDIR tmpfs defaults 1 1" >> /etc/fstab && mount "$LOGDIR" )

echo 'Установка cgi обработчика языка Parser'
mkdir -p /usr/local/lib/parser 2>/dev/null \
    && wget -qO- http://www.parser.ru/off-line/download/parser343/parser3_4_3_debian6.tar.gz | tar xz -C /usr/local/lib/parser \
    && chown -R www-data:www-data /usr/local/lib/parser && chmod 755 /usr/local/lib/parser/parser3.cgi
cp -R /usr/local/lib/parser/parser3.cgi /usr/local/lib/parser/lib /vagrant/dealer.git/shared/www/cgi-bin

echo 'Настройка PHP'
mkdir -p /usr/local/bin 2>/dev/null
wget -qO/usr/local/bin/sendmail.sh https://gist.github.com/AntonTyutin/8254685/raw/764eaf7ab18ded4edb2ef4acaf50d5c48e2d62ba/sendmail.sh \
    && chmod 755 /usr/local/bin/sendmail.sh
cp $PROVISION_DIR/xdebug-config.ini /etc/php5/apache2/conf.d/21-xdebug-config.ini

echo 'Настройка web-сервера'
rm /etc/apache2/sites-enabled/000-default.conf 2>/dev/null
cp $PROVISION_DIR/vagrant.apache.conf /etc/apache2/sites-enabled/000-default.conf
ln -s ../../phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf
ln -s ../conf-available/phpmyadmin.conf /etc/apache2/conf-enabled/
a2enmod actions 2>&1 >/dev/null
a2enmod rewrite 2>&1 >/dev/null
a2enmod cgi 2>&1 >/dev/null
service apache2 restart

echo 'Настройка БД'
sed -ri 's/(bind-address\s+=\s+)127.0.0.1/\10.0.0.0/' /etc/mysql/my.cnf
mysql -uroot -e 'create user "root"@"192.168.88.1"; grant all privileges on *.* to "root"@"192.168.88.1"'
service mysql restart
bzcat $PROVISION_DIR/initial.sql.bz2 | mysql -uroot

cp $PROVISION_DIR/dealer/* /vagrant/dealer.git/custom/config
cp $PROVISION_DIR/importer/* /vagrant/importer.git/config
cp $PROVISION_DIR/pma-config.php /etc/phpmyadmin/conf.d/

echo "127.0.0.1  $DOMAIN dealer.$DOMAIN importer.$DOMAIN" >> /etc/hosts

cat <<MSG
Установка завершена!

Добавьте в свой /etc/hosts файл строку:
echo "  192.168.88.11  $DOMAIN dealer.$DOMAIN importer.$DOMAIN" | sudo tee -a /etc/hosts

Теперь разработческие сайты доступны по адресам 
http://dlr.$DOMAIN http://imp.$DOMAIN,
а PhpMyAdmin доступен по адресу http://$DOMAIN/phpmyadmin/

Логи сервера можно получиить по адресу http://$DOMAIN/.logs/
Логи уничтожаются после перезагрузки виртуальной машины.
MSG
