<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

return [
    'runtimePath' => sys_get_temp_dir(),
    'params' => [
        'reference_database_name' => 'autocrm',    // имя БД справочников
        'importer_database_name'  => 'autocrm_gm',   // имя БД импортера для синхронизации данных
        'clients_database_name'   => 'clients',
    ],
    'components' => [
        'dataIntegrityErrorReporter' => ['class' => 'Infotech\Autocrm\Common\Components\WarningReporterStub'],
        'db' => [
            'connectionString'  => 'mysql:host=autocrm.dev;dbname=dealer',
            'username'          => 'root',
            'password'          => '',

            'emulatePrepare'    => true,
            'charset'           => 'utf8',
//            'enableProfiling'   => true, // включаем профайлер
            'enableParamLogging' => true, // показываем значения параметров
        ],
    ]
];
