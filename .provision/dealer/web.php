<?php

define('AUTOCRM_LOG_PATH', '/var/log/www');

return [
    'components' => [
        'log' => [
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'warning, error',
                    'logPath' => AUTOCRM_LOG_PATH,
                    'logFile' => 'dealer-app.log',
//                    'categories' => 'system.*',
                    'enabled' => true
                ],
                [
                    'class' => 'CFileLogRoute',
                    'logPath' => AUTOCRM_LOG_PATH,
                    'logFile' => 'dealer-db.log',
                    'categories' => 'system.db.*',
                    'enabled' => false
                ],
            ]
        ],
    ],
];
